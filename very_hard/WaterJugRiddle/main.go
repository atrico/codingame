package main

import (
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	//reader := os.Stdin
	reader := test1
	target, jugs := read(reader)
	fmt.Fprintln(os.Stderr, target, jugs)

	q := Queue[State]{}
	st := NewState(jugs)
	q.Push(st)
	used := make(Set[int])
	used.Add(st.Hash())
	for !q.IsEmpty() {
		// Get next state
		current := q.Pop()
		// Possible moves
		var canFill []int // Has space to fill
		var canPour []int // Has content to pour
		for i, jug := range current.jugState {
			if jug > 0 {
				canPour = append(canPour, i)
			}
			if jug < jugs[i] {
				canFill = append(canFill, i)
			}
		}
		// Pour?
		for _, i := range canPour {
			for _, j := range canFill {
				if i != j {
					next := current.Pour(i, j)
					if hsh := next.Hash(); !used.Contains(hsh) {
						if next.Solved(target) {
							fmt.Fprintln(os.Stderr, next.moves)
							fmt.Println(len(next.moves))
							return
						}
						q.Push(next)
						used.Add(hsh)
					}
				}
			}
		}
		// Fill
		for _, i := range canFill {
			next := current.Fill(i)
			if hsh := next.Hash(); !used.Contains(hsh) {
				q.Push(next)
				used.Add(hsh)
			}
		}
		// Empty
		for _, i := range canPour {
			next := current.Empty(i)
			if hsh := next.Hash(); !used.Contains(hsh) {
				q.Push(next)
				used.Add(hsh)
			}
		}
	}

	fmt.Println("oops")
}

func read(reader io.Reader) (target int, jugs []int) {
	fmt.Fscan(reader, &target)

	var containersCount int
	fmt.Fscan(reader, &containersCount)
	jugs = make([]int, containersCount)
	for i := 0; i < containersCount; i++ {
		fmt.Fscan(reader, &jugs[i])
	}
	return
}

// ----------------------------------------------------------------------------------------------------------------------------
// State
// ----------------------------------------------------------------------------------------------------------------------------

type State struct {
	jugs     []int
	jugState []int
	moves    []string
}

func NewState(jugs []int) State {
	return State{jugs: jugs, jugState: make([]int, len(jugs))}
}

func (s State) Pour(i, j int) (state State) {
	state = s.Copy()
	transfer := min(s.jugs[j]-s.jugState[j], s.jugState[i])
	state.jugState[i] = s.jugState[i] - transfer
	state.jugState[j] = s.jugState[j] + transfer
	state.moves = append(state.moves, fmt.Sprintf("Pour: %d,%d", s.jugs[i], s.jugs[j]))
	return
}

func (s State) Fill(i int) (state State) {
	state = s.Copy()
	state.jugState[i] = s.jugs[i]
	state.moves = append(state.moves, fmt.Sprintf("Fill: %d", s.jugs[i]))
	return
}

func (s State) Empty(i int) (state State) {
	state = s.Copy()
	state.jugState[i] = 0
	state.moves = append(state.moves, fmt.Sprintf("Empty: %d", s.jugs[i]))
	return
}

func (s State) Solved(target int) bool {
	for _, j := range s.jugState {
		if j == target {
			return true
		}
	}
	return false
}

func (s State) Hash() (hsh int) {
	hsh = 73
	for _, i := range s.jugState {
		hsh = 37*hsh + i

	}
	return
}

func (s State) Copy() (cpy State) {
	cpy.jugs = s.jugs
	cpy.jugState = make([]int, len(s.jugState))
	copy(cpy.jugState, s.jugState)
	cpy.moves = make([]string, len(s.moves), len(s.moves)+1)
	copy(cpy.moves, s.moves)
	return
}

// ----------------------------------------------------------------------------------------------------------------------------
// Queue
// ----------------------------------------------------------------------------------------------------------------------------

type Queue[T any] struct {
	l []T
}

func (q *Queue[T]) Push(item T) {
	q.l = append(q.l, item)
}

func (q *Queue[T]) Pop() (item T) {
	if q.IsEmpty() {
		panic("queue is empty")
	}
	item = q.l[0]
	q.l = q.l[1:]
	return
}

func (q *Queue[T]) IsEmpty() bool {
	return len(q.l) == 0
}

// ----------------------------------------------------------------------------------------------------------------------------
// Set (simplified)
// ----------------------------------------------------------------------------------------------------------------------------

type Set[T comparable] map[T]any

func (s *Set[T]) Add(item T) {
	(*s)[item] = nil
}

func (s *Set[T]) Contains(item T) (ok bool) {
	_, ok = (*s)[item]
	return
}

// ----------------------------------------------------------------------------------------------------------------------------
// Misc
// ----------------------------------------------------------------------------------------------------------------------------

func min(i, j int) int {
	if j < i {
		return j
	}
	return i
}

// ----------------------------------------------------------------------------------------------------------------------------
// Examples
// ----------------------------------------------------------------------------------------------------------------------------

var test1 = strings.NewReader("4\n2\n3\n5\n")
