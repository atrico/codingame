module gitlab.com/atrico/codingame

go 1.22.3

require gitlab.com/atrico/testing/v2 v2.5.2

require golang.org/x/exp v0.0.0-20240416160154-fe59bbe5cc7f // indirect
