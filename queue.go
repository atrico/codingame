package codingame

type Queue[T any] struct {
	l []T
}

func (q *Queue[T]) Push(item T) {
	q.l = append(q.l, item)
}

func (q *Queue[T]) Pop() (item T) {
	if q.IsEmpty() {
		panic("queue is empty")
	}
	item = q.l[0]
	q.l = q.l[1:]
	return
}

func (q *Queue[T]) IsEmpty() bool {
	return len(q.l) == 0
}
