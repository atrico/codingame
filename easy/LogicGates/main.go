package main

import (
	"fmt"
	"os"
	"strings"
)

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

func main() {
	var n int
	fmt.Scan(&n)

	var m int
	fmt.Scan(&m)

	signals := make(map[string]string)

	// Inputs
	for i := 0; i < n; i++ {
		var inputName, inputSignal string
		fmt.Scan(&inputName, &inputSignal)
		signals[inputName] = inputSignal
		fmt.Fprintln(os.Stderr, inputName, inputSignal)
	}
	// Ouputs
	var display []string
	for i := 0; i < m; i++ {
		var outputName, _type, inputName1, inputName2 string
		fmt.Scan(&outputName, &_type, &inputName1, &inputName2)
		display = append(display, outputName)
		fmt.Fprintln(os.Stderr, fmt.Sprintf("%s = %s %s %s", outputName, inputName1, _type, inputName2))
		// Calculate signal
		signals[outputName] = trace(gates[_type], signals[inputName1], signals[inputName2])
	}
	for i := 0; i < m; i++ {
		fmt.Println(display[i], signals[display[i]])
	}
}

func trace(gate Gate, lhs, rhs string) string {
	str := strings.Builder{}
	for i, l := range lhs {
		str.WriteString(string(gate(l, rune(rhs[i]))))
	}
	return str.String()
}

type Gate func(lhs, rhs rune) rune

var gates = map[string]Gate{
	"AND": AND,
	"OR":  OR,
	"XOR": XOR,
	"NAND": func(lhs, rhs rune) rune {
		return NOT(AND(lhs, rhs))
	},
	"NOR": func(lhs, rhs rune) rune {
		return NOT(OR(lhs, rhs))
	},
	"NXOR": func(lhs, rhs rune) rune {
		return NOT(XOR(lhs, rhs))
	},
}

func AND(lhs, rhs rune) rune {
	if lhs == '-' && rhs == '-' {
		return '-'
	}
	return '_'
}

func OR(lhs, rhs rune) rune {
	if lhs == '-' || rhs == '-' {
		return '-'
	}
	return '_'
}

func XOR(lhs, rhs rune) rune {
	if lhs != rhs {
		return '-'
	}
	return '_'
}

func NOT(lhs rune) rune {
	if lhs == '-' {
		return '_'
	}
	return '-'
}
