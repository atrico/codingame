package main

import (
	"fmt"
	"math"
	"os"
)

const (
	Gravity     = 3.711
	GravitySqrd = Gravity * Gravity
	VMax        = 40.0
	HMax        = 20.0
)

func main() {
	// Scan surface
	surface := new(Surface)
	surface.Read()
	// Find landing spot
	landMin, landMax, landY := surface.GetLandingZone()
	fmt.Fprintf(os.Stderr, "Landing zone = %d - %d, %d\n", int(landMin), int(landMax), int(landY))
	// Landing loop
	lander := new(Lander)
	var controls []Control
	idx := 0
	for {
		// Update lander
		lander.Update()
		// Debug
		//lander.Debug()
		// Next control input
		if idx == 0 {
			controls = lander.GetControl(landY, VMax)
		}
		fmt.Println(controls[idx])
		idx++
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// State
// ----------------------------------------------------------------------------------------------------------------------------

type State struct {
	position Coords
	// hSpeed: the horizontal speed (in m/s), towards top/right
	// vSpeed: the vertical speed (in m/s), towards top/right
	hSpeed, vSpeed float64
	// fuel: the quantity of remaining fuel in litres.
	fuel int
}

func (s State) String() string {
	return fmt.Sprintf("pos=%v, HSpeed=%d, VSpeed=%d, Fuel=%d", s.position, int(s.hSpeed), int(s.vSpeed), s.fuel)
}

func (s State) Copy() State {
	return State{
		position: s.position.Copy(),
		hSpeed:   s.hSpeed,
		vSpeed:   s.vSpeed,
		fuel:     s.fuel,
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Control structure
// ----------------------------------------------------------------------------------------------------------------------------

type Control struct {
	// rotate: the rotation angle in degrees (-90 to 90).
	rotate int
	// power: the thrust power (0 to 4).
	power float64
}

func (c Control) Copy() Control {
	return Control{c.rotate, c.power}
}
func (c Control) String() string {
	return fmt.Sprintf("%d %d", c.rotate, 0)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Lander
// ----------------------------------------------------------------------------------------------------------------------------

type Lander struct {
	state   State
	control Control
}

func (l *Lander) Update() {
	fmt.Scan(&l.state.position.x, &l.state.position.y, &l.state.hSpeed, &l.state.vSpeed, &l.state.fuel, &l.control.rotate, &l.control.power)
}

// SUVAT
// v = u + at
// s = ut + ½at²
// v² = u² + 2as
func (l *Lander) GetControl(yTarget float64, vTargetSpeed float64) (control []Control) {
	// Vertical component
	projection := new(Projection)
	projection.Project(l.state, l.control, yTarget, vTargetSpeed)
	// TODO - horizontal
	current := projection.stages.next
	for current != nil {
		control = append(control, current.control)
		current = current.next
	}
	return
}

func (l *Lander) Debug() {
	fmt.Fprintf(os.Stderr, "%v\n%v\n", l.state, l.control)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Projection
// ----------------------------------------------------------------------------------------------------------------------------

type Stage struct {
	control    Control
	state      State
	calculated bool
	prev       *Stage
	next       *Stage
}

func (s *Stage) Append(control Control) *Stage {
	s.next = &Stage{control: control}
	s.next.prev = s
	return s.next
}

func (s *Stage) Calculate(force bool) {
	if !s.calculated || force {
		prev := s.prev.state
		power := s.control.power
		// Update for 1s
		s.state = prev.Copy()
		s.state.position.y += prev.vSpeed + (power-Gravity)/2.0
		s.state.vSpeed += power - Gravity
		s.calculated = true
		// Next stage is invalidated
		if s.next != nil {
			s.next.calculated = false
			s.next.Calculate(false)
		}
	}
}

type Projection struct {
	stages *Stage
}

func (p *Projection) Project(state State, control Control, yTarget float64, vTargetSpeed float64) {
	// Start with initial state
	p.stages = &Stage{
		control:    control.Copy(),
		state:      state.Copy(),
		calculated: true,
	}
	todoCount := 0
	// Add full acceleration until reach target
	last := p.stages
	for last.state.position.y > yTarget {
		last = last.Append(Control{})
		todoCount++
		last.Calculate(true)
	}
	// Backwards add deceleration
	current := last
	for math.Abs(last.state.vSpeed) > math.Abs(vTargetSpeed) && current != nil {
		current.control.power = 4
		current.Calculate(true)
		current = current.prev
	}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Surface
// ----------------------------------------------------------------------------------------------------------------------------

type Coords struct {
	x, y float64
}

func (c Coords) Copy() Coords {
	return Coords{c.x, c.y}
}

func (c Coords) String() string {
	return fmt.Sprintf("(%d,%d)", int(c.x), int(c.y))
}

type Surface struct {
	land []Coords
}

func (s *Surface) Read() {
	// surfaceN: the number of points used to draw the surface of Mars.
	var surfaceN int
	fmt.Scan(&surfaceN)
	s.land = make([]Coords, surfaceN)
	for i := 0; i < surfaceN; i++ {
		var landX, landY int
		fmt.Scan(&landX, &landY)
		s.land[i] = Coords{float64(landX), float64(landY)}
	}
	for _, c := range s.land {
		fmt.Fprintln(os.Stderr, c)
	}
}

func (s *Surface) GetLandingZone() (landMin, landMax, landY float64) {
	for i := 1; i < len(s.land); i++ {
		if s.land[i-1].y == s.land[i].y {
			return s.land[i-1].x, s.land[i].x, s.land[i].y
		}
	}
	panic("Landing zone not found")
}
