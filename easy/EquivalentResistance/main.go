package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
var lookup map[string]int

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, 1000000), 1000000)

	var N int
	scanner.Scan()
	fmt.Sscan(scanner.Text(), &N)

	lookup = make(map[string]int)
	for i := 0; i < N; i++ {
		var name string
		var R int
		scanner.Scan()
		fmt.Sscan(scanner.Text(), &name, &R)
		lookup[name] = R
		fmt.Fprintln(os.Stderr, fmt.Sprintf("%s = %d", name, R))
	}

	scanner.Scan()
	circuit := scanner.Text()
	fmt.Fprintln(os.Stderr, circuit)

	item, _ := Parse(strings.Split(circuit, " "))

	fmt.Println(fmt.Sprintf("%0.1f", item.Evaluate()))
}

func Parse(list []string) (item Item, newList []string) {
	if len(list) > 0 {
		newList = list[1:]
		switch list[0] {
		case "(", "[":
			var items []Item
			var next Item
			next, newList = Parse(newList)
			for next != nil {
				items = append(items, next)
				next, newList = Parse(newList)
			}
			if list[0] == "(" {
				item = itemSeries(items)
			} else {
				item = itemParallel(items)
			}
		case ")", "]":
			// Nothing (return)
		default:
			// Lookup value
			item = itemVal(lookup[list[0]])
		}
	}
	return
}

type Item interface {
	Evaluate() (val float64)
}

type itemVal int

func (i itemVal) Evaluate() (val float64) {
	val = float64(i)
	return
}

type itemSeries []Item

func (i itemSeries) Evaluate() (val float64) {
	for _, v := range i {
		val += v.Evaluate()
	}
	return
}

type itemParallel []Item

func (i itemParallel) Evaluate() (val float64) {
	for _, v := range i {
		val += 1.0 / v.Evaluate()
	}
	return 1.0 / val
}
