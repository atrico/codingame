package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

/**
 * 6 Degrees of Kevin Bacon!
 **/

func main() {
	actorName, movies := read()
	fmt.Fprintln(os.Stderr, actorName)
	for movie, actors := range movies {
		fmt.Fprintln(os.Stderr, movie, actors)
	}
	// Build graph
	graph := make(map[string][]ActorAndMovie)
	for movie, actors := range movies {
		for _, actor1 := range actors {
			for _, actor2 := range actors {
				if actor1 != actor2 {
					graph[actor1] = append(graph[actor1], ActorAndMovie{actor2, movie})
				}
			}
		}
	}
	// Breadth first traversal
	var queue Queue[ActorAndLinks]
	queue.Push(ActorAndLinks{actor: actorName})
	for !queue.IsEmpty() {
		current := queue.Pop()
		// Check for complete
		if current.actor == "Kevin Bacon" {
			fmt.Fprintln(os.Stderr, current.links)
			fmt.Println(len(current.links))
			return
		}
		// Push links
		for _, link := range graph[current.actor] {
			newList := make([]ActorAndMovie, len(current.links)+1)
			copy(newList, current.links)
			newList[len(current.links)] = link
			queue.Push(ActorAndLinks{actor: link.actor, links: newList})
		}
	}
	fmt.Println("-1")
}

func read() (actorName string, movies map[string][]string) {
	movies = make(map[string][]string)
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, 1000000), 1000000)

	scanner.Scan()
	actorName = scanner.Text()
	var n int
	scanner.Scan()
	fmt.Sscan(scanner.Text(), &n)

	for i := 0; i < n; i++ {
		scanner.Scan()
		parts := strings.Split(scanner.Text(), ": ")
		movies[parts[0]] = strings.Split(parts[1], ", ")
	}
	return
}

type ActorAndMovie struct {
	actor string
	movie string
}

type ActorAndLinks struct {
	actor string
	links []ActorAndMovie
}

type Queue[T any] struct {
	l []T
}

func (q *Queue[T]) Push(item T) {
	q.l = append(q.l, item)
}

func (q *Queue[T]) Pop() (item T) {
	if q.IsEmpty() {
		panic("queue is empty")
	}
	item = q.l[0]
	q.l = q.l[1:]
	return
}

func (q *Queue[T]) IsEmpty() bool {
	return len(q.l) == 0
}
