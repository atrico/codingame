package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	var N int
	fmt.Scan(&N)

	tree := make([]Player, N)

	for i := 0; i < N; i++ {
		var NUMPLAYER int
		var SIGNPLAYER string
		fmt.Scan(&NUMPLAYER, &SIGNPLAYER)
		tree[i] = Player{num: NUMPLAYER, sign: SIGNPLAYER}
	}
	// Play
	for len(tree) > 1 {
		newTree := make([]Player, len(tree)/2)
		for i := 0; i < len(tree); i += 2 {
			newTree[i/2] = winner(tree[i], tree[i+1])
		}
		tree = newTree
	}
	fmt.Println(tree[0])
}

type Player struct {
	num     int
	sign    string
	history []int
}

func (p Player) hasWon(opp Player) Player {
	return Player{p.num, p.sign, append(p.history, opp.num)}
}

func (p Player) String() string {
	history := strings.Builder{}
	notFirst := false
	for _, h := range p.history {
		if notFirst {
			history.WriteString(" ")
		}
		history.WriteString(strconv.Itoa(h))
		notFirst = true
	}
	return fmt.Sprintf("%d\n%s", p.num, history.String())
}

func winner(p0, p1 Player) Player {
	swap := false
	if p0.sign == p1.sign {
		swap = p1.num < p0.num
	} else {
		_, swap = winners[fmt.Sprintf("%s%s", p1.sign, p0.sign)]
	}
	if swap {
		p0, p1 = p1, p0
	}
	return p0.hasWon(p1)
}

var winners = map[string]any{
	"CP": nil,
	"PR": nil,
	"RL": nil,
	"LS": nil,
	"SC": nil,
	"CL": nil,
	"LP": nil,
	"PS": nil,
	"SR": nil,
	"RC": nil,
}
