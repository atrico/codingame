package main

import (
	"fmt"
	"io"
)
import "os"
import "bufio"
import "strings"
import "strconv"

func main() {
	//reader := os.Stdin
	reader := test1
	columns, rows := read(reader)
	fmt.Fprintln(os.Stderr, columns, rows)

	combX := combinations(columns)
	combY := combinations(rows)
	fmt.Fprintln(os.Stderr, combX, combY)

	result := 0
	mp1, mp2 := combX, combY
	// Use smallest
	if len(mp1) > len(mp2) {
		mp1, mp2 = mp2, mp1
	}
	for k, v := range mp1 {
		if v2, ok := mp2[k]; ok {
			result += v * v2
		}
	}

	fmt.Println(result) // Write answer to stdout
}

func read(reader io.Reader) (columns, rows []int) {
	scanner := bufio.NewScanner(reader)
	scanner.Buffer(make([]byte, 1000000), 1000000)
	var inputs []string

	var w, h, countX, countY int
	scanner.Scan()
	fmt.Sscan(scanner.Text(), &w, &h, &countX, &countY)
	fmt.Fprintln(os.Stderr, w, h, countX, countY)
	columns = make([]int, countX, countX+1)
	rows = make([]int, countY, countY+1)

	scanner.Scan()
	inputs = strings.Split(scanner.Text(), " ")
	last := 0
	for i := 0; i < countX; i++ {
		x, _ := strconv.ParseInt(inputs[i], 10, 32)
		fmt.Fprint(os.Stderr, x, " ")
		columns[i] = int(x) - last
		w -= columns[i]
		last = int(x)
	}
	fmt.Fprintln(os.Stderr)
	columns = append(columns, w)

	scanner.Scan()
	inputs = strings.Split(scanner.Text(), " ")
	last = 0
	for i := 0; i < countY; i++ {
		y, _ := strconv.ParseInt(inputs[i], 10, 32)
		fmt.Fprint(os.Stderr, y, " ")
		rows[i] = int(y) - last
		h -= rows[i]
		last = int(y)
	}
	fmt.Fprintln(os.Stderr)
	rows = append(rows, h)
	return
}

// Get all widths/height from set of lines
func combinations(orig []int) (combs map[int]int) {
	combs = make(map[int]int)
	for i := range orig {
		for k, v := range combinations2(orig[i:]) {
			combs[k] = combs[k] + v
		}
	}
	return
}

func combinations2(orig []int) (combs map[int]int) {
	combs = make(map[int]int)
	if len(orig) > 0 {
		current := orig[0]
		combs[current] = combs[current] + 1
		for k, v := range combinations2(orig[1:]) {
			combs[current+k] = combs[current+k] + v
		}
	}
	return
}

var test1 = strings.NewReader("10 5 2 1\n2 5\n3")
