package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func main() {
	run(bufio.NewReader(os.Stdin), os.Stdout)
}

func run(reader io.Reader, writer io.Writer) {
	r, l := read(reader)
	result := solve(r, l)
	fmt.Fprintln(writer, format(result))
}

func format(result []int) string {
	str := make([]string, len(result))
	for i, r := range result {
		str[i] = strconv.Itoa(r)
	}
	return strings.Join(str, " ")
}

func read(reader io.Reader) (R, L int) {
	fmt.Fscan(reader, &R, &L)
	fmt.Fprintln(os.Stderr, R, L)
	return
}

func solve(r, l int) (line []int) {
	line = []int{r}
	for i := 1; i < l; i++ {
		line = iterate(line)
	}
	return
}

func iterate(line []int) (result []int) {
	val, count := 0, 0
	for _, item := range line {
		if item == val {
			count++
		} else {
			if count > 0 {
				result = append(result, count, val)
			}
			val = item
			count = 1
		}
	}
	result = append(result, count, val)
	return
}
