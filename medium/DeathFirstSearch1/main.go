package main

import (
	"fmt"
	"os"
)

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

func main() {
	// N: the total number of nodes in the level, including the gateways
	// L: the number of links
	// E: the number of exit gateways
	var N, L, E int
	fmt.Scan(&N, &L, &E)
	Debug(N, L, E)

	graph := NewGraph(N)

	for i := 0; i < L; i++ {
		// N1: N1 and N2 defines a link between these nodes
		var N1, N2 int
		fmt.Scan(&N1, &N2)
		graph.AddLink(N1, N2)
		Debug(N1, N2)
	}
	for i := 0; i < E; i++ {
		// EI: the index of a gateway node
		var EI int
		fmt.Scan(&EI)
		graph.SetGateway(EI)
		Debug(EI)
	}
	for {
		// SI: The index of the node on which the Skynet agent is positioned this turn
		var SI int
		fmt.Scan(&SI)
		Debug("Agent = ", SI)

		// Find nearest gateway to agent and kill link
		node1, node2 := graph.GetNearestGateway(SI)
		Debug("Cut = ", node1, " - ", node2)

		// Example: 0 1 are the indices of the nodes you wish to sever the link between
		fmt.Println(fmt.Sprintf("%d %d", node1, node2))

		// Update graph
		graph.CutLink(node1, node2)
	}
}

func Debug(msg ...interface{}) {
	fmt.Fprintln(os.Stderr, msg...)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Graph
// ----------------------------------------------------------------------------------------------------------------------------

type graph []Node

type Node struct {
	IsGateway bool
	Links     IntSet
}

type Graph interface {
	AddLink(node1, node2 int) Graph
	CutLink(node1, node2 int) Graph
	SetGateway(idx int) Graph
	GetNearestGateway(node int) (previous, gateway int)
}

func NewGraph(nodes int) Graph {
	g := make(graph, nodes)
	for i := range g {
		g[i] = Node{false, NewIntSet()}
	}
	return &g
}

func (g *graph) AddLink(node1, node2 int) Graph {
	(*g)[node1].Links.Add(node2)
	(*g)[node2].Links.Add(node1)
	return g
}

func (g *graph) CutLink(node1, node2 int) Graph {
	(*g)[node1].Links.Remove(node2)
	(*g)[node2].Links.Remove(node1)
	return g
}

func (g *graph) SetGateway(idx int) Graph {
	(*g)[idx].IsGateway = true
	return g
}

func (g *graph) GetNearestGateway(node int) (previous, gateway int) {
	if !(*g)[node].IsGateway {
		// Walk graph to nearest gateway
		visited := NewIntSet(node)
		pending := NewIntQueue(node)
		for !pending.IsEmpty() {
			current := pending.Pop()
			for lnk := range (*g)[current].Links {
				if !visited.Contains(lnk) {
					if (*g)[lnk].IsGateway {
						return current, lnk
					}
					visited.Add(lnk)
					pending.Push(lnk)
				}
			}
		}
	}
	return -1, -1
}

// ----------------------------------------------------------------------------------------------------------------------------
// Queue
// ----------------------------------------------------------------------------------------------------------------------------

type IntQueue struct {
	Queue []int
	Count int
}

func NewIntQueue(initial ...int) IntQueue {
	q := IntQueue{Queue: make([]int, len(initial)), Count: len(initial)}
	for i, el := range initial {
		q.Queue[i] = el
	}
	return q
}

func (q IntQueue) IsEmpty() bool {
	return q.Count == 0
}

func (q *IntQueue) Push(el int) {
	q.Queue = append(q.Queue, el)
	q.Count++
}

func (q *IntQueue) Pop() int {
	if q.IsEmpty() {
		panic("Queue is IsEmpty")
	}
	val := q.Queue[0]
	q.Queue = q.Queue[1:]
	q.Count--
	return val
}

// ----------------------------------------------------------------------------------------------------------------------------
// Set
// ----------------------------------------------------------------------------------------------------------------------------
var _blank struct{}

type IntSet map[int]struct{}

func NewIntSet(initial ...int) IntSet {
	s := make(IntSet)
	for _, el := range initial {
		s.Add(el)
	}
	return s
}

func (s *IntSet) Add(el int) {
	(*s)[el] = _blank
}

func (s *IntSet) Remove(el int) {
	delete(*s, el)
}

func (s IntSet) Contains(el int) bool {
	_, ok := s[el]
	return ok
}
