package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main() {
	run(bufio.NewReader(os.Stdin), os.Stdout)
}

func run(reader io.Reader, writer io.Writer) {
	tree := read(reader)
	// The number of people involved in the longest succession of influences
	fmt.Fprintln(writer, solve(tree))
}

func read(reader io.Reader) map[int][]int {
	mp := make(map[int][]int)
	// n: the number of relationships of influence
	var n int
	fmt.Fscan(reader, &n)
	debug(n)
	for i := 0; i < n; i++ {
		// x: a relationship of influence between two people (x influences y)
		var x, y int
		fmt.Fscan(reader, &x, &y)
		debug(x, y)
		mp[x] = append(mp[x], y)
	}
	return mp
}

func solve(tree map[int][]int) (mx int) {
	return measure(tree, -1, 0)
}

func measure(tree map[int][]int, val int, count int) (mx int) {
	var children []int
	if val == -1 {
		children = make([]int, 0, len(tree))
		for val, _ = range tree {
			children = append(children, val)
		}
	} else {
		children = tree[val]
	}
	if len(children) == 0 {
		mx = count
	} else {
		for _, next := range children {
			mx = max(mx, measure(tree, next, count+1))
		}
	}
	return
}

func debug(text ...any) {
	fmt.Fprintln(os.Stderr, text...)
}

func max(l, r int) int {
	if r > l {
		return r
	}
	return l
}
