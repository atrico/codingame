package main

import (
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"strings"
	"testing"
)

func Test(t *testing.T) {
	for name, testCase := range TestCases {
		t.Run(name, func(t *testing.T) {
			func(testCase TestCase) {
				// Arrange
				reader := strings.NewReader(testCase.input)
				writer := strings.Builder{}

				// Act
				run(reader, &writer)

				// Assert
				assert.That(t, writer.String(), is.EqualTo(testCase.expected), "")
			}(testCase)
		})
	}
}

type TestCase struct {
	input, expected string
}

var TestCases = map[string]TestCase{
	"01.Simple example":    {"3\n1 2\n1 3\n3 4", "3\n"},
	"04.Several mentors 2": {"9\n7 2\n8 9\n1 6\n6 9\n1 7\n1 2\n3 9\n2 3\n6 3", "5\n"},
}

// Benchmark-20    	 4844140	       222.9 ns/op
// Benchmark-20    	 4611066	       222.5 ns/op
// Benchmark-20    	 4553836	       229.1 ns/op
// Benchmark-20    	 5149423	       232.2 ns/op
// Benchmark-20    	 5776890	       217.5 ns/op
//
//	Average          4987071           224.8
func Benchmark(b *testing.B) {
	tree := map[int][]int{
		1: {2, 6, 7},
		2: {3},
		3: {9},
		6: {3, 9},
		7: {2},
		8: {9},
	}
	for i := 0; i < b.N; i++ {
		solve(tree)
	}
}
