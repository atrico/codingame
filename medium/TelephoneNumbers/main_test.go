package main

import (
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"strings"
	"testing"
)

func Test(t *testing.T) {
	for name, testCase := range TestCases {
		t.Run(name, func(t *testing.T) {
			func(testCase TestCase) {
				// Arrange
				reader := strings.NewReader(testCase.input)
				writer := strings.Builder{}

				// Act
				run(reader, &writer)

				// Assert
				assert.That(t, writer.String(), is.EqualTo(testCase.expected), "")
			}(testCase)
		})
	}
}

type TestCase struct {
	input, expected string
}

var TestCases = map[string]TestCase{
	"01.A telephone number":            {"1\n0467123456", "10\n"},
	"02.Numbers with a different base": {"2\n0123456789\n1123456789", "20\n"},
	"03.Number included in another":    {"2\n0123456789\n0123", "10\n"},
	"04.Numbers with a common part":    {"5\n0412578440\n0412199803\n0468892011\n112\n15", "28\n"},
}
