package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main() {
	run(bufio.NewReader(os.Stdin), os.Stdout)
}

func run(reader io.Reader, writer io.Writer) {
	numbers := read(reader)
	result := solve(numbers)
	// The number of elements (referencing a number) stored in the structure.
	fmt.Fprintln(writer, result)
}

func read(reader io.Reader) (numbers []string) {
	var N int
	fmt.Fscan(reader, &N)
	fmt.Fprintln(os.Stderr, N)
	numbers = make([]string, N)

	for i := 0; i < N; i++ {
		var telephone string
		fmt.Fscan(reader, &telephone)
		fmt.Fprintln(os.Stderr, telephone)
		numbers[i] = telephone
	}
	return
}

func solve(numbers []string) (count int) {
	root := new(Node)
	for _, number := range numbers {
		count += add(root, []rune(number))
	}
	return count
}

type Node struct {
	key      rune
	children []*Node
}

func add(node *Node, number []rune) (added int) {
	// Terminate when number is done
	if len(number) == 0 {
		return 0
	}
	// Find next digit in children
	for _, n := range node.children {
		if n.key == number[0] {
			return add(n, number[1:])
		}
	}
	// Add new node for digit
	newNode := &Node{key: number[0]}
	node.children = append(node.children, newNode)
	return 1 + add(newNode, number[1:])
}
