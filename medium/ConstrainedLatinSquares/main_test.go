package main

import (
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"strings"
	"testing"
)

func Test(t *testing.T) {
	for name, testCase := range TestCases {
		t.Run(name, func(t *testing.T) {
			func(testCase TestCase) {
				// Arrange
				reader := strings.NewReader(testCase.input)
				writer := strings.Builder{}

				// Act
				run(reader, &writer)

				// Assert
				assert.That(t, writer.String(), is.EqualTo(testCase.expected), "")
			}(testCase)
		})
	}
}

type TestCase struct {
	input, expected string
}

var TestCases = map[string]TestCase{
	"01.3x3 Easy":          {"3\n123\n200\n300", "1\n"},
	"02.4x4 Unconstrained": {"4\n0000\n0000\n0000\n0000", "576\n"},
	"03.5x5 Wrong grid":    {"5\n10001\n00000\n00000\n00000\n00000", "0\n"},
	"04.5x5 Fundamental":   {"5\n12345\n20000\n30000\n40000\n50000", "56\n"},
	"05.6x6":               {"6\n100000\n020000\n003000\n000400\n000050\n000006", "10752\n"},
	"06.7x7":               {"7\n2000007\n0403000\n0030000\n0006004\n1500000\n0000020\n0100070", "14388\n"},
	"07.8x8":               {"8\n12345600\n23456000\n34560000\n45600000\n56000000\n60000000\n00000000\n00000008", "1\n"},
	"08.9x9":               {"9\n420789036\n200000003\n004000000\n900123007\n500231008\n700312009\n000000000\n300000004\n630907025", "8896\n"},
	"09.9x9 Hard":          {"9\n087604321\n800000002\n700000003\n600523000\n000302005\n401230006\n300000007\n200000008\n103050780\n0", "44032\n"},
}

// Benchmark-20    	       1	14196635900 ns/op
// Benchmark-20    	       1	12481539900 ns/op
// Benchmark-20    	       1	11887421700 ns/op
// Average                      12855199167
func Benchmark(b *testing.B) {
	grid := Grid{
		[][]int{
			{0, 8, 7, 6, 0, 4, 3, 2, 1},
			{8, 0, 0, 0, 0, 0, 0, 0, 2},
			{7, 0, 0, 0, 0, 0, 0, 0, 3},
			{6, 0, 0, 5, 2, 3, 0, 0, 0},
			{0, 0, 0, 3, 0, 2, 0, 0, 5},
			{4, 0, 1, 2, 3, 0, 0, 0, 6},
			{3, 0, 0, 0, 0, 0, 0, 0, 7},
			{2, 0, 0, 0, 0, 0, 0, 0, 8},
			{1, 0, 3, 0, 5, 0, 7, 8, 0},
		},
	}
	for i := 0; i < b.N; i++ {
		solve(grid)
	}
}
