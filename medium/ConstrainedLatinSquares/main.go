package main

import (
	"fmt"
	"io"
	"strconv"
)
import "os"
import "bufio"

func main() {
	run(bufio.NewReader(os.Stdin), os.Stdout)
}

func run(reader io.Reader, writer io.Writer) {
	grid := read(reader)
	result := solve(grid)
	fmt.Fprintln(writer, result) // Write answer to stdout
}

func read(reader io.Reader) (grid Grid) {
	scanner := bufio.NewScanner(reader)
	scanner.Buffer(make([]byte, 1000000), 1000000)

	var n int
	scanner.Scan()
	fmt.Sscan(scanner.Text(), &n)
	fmt.Fprintln(os.Stderr, n)
	grid = Grid{make([][]int, n)}

	for i := 0; i < n; i++ {
		scanner.Scan()
		row := scanner.Text()
		fmt.Fprintln(os.Stderr, row)
		grid.g[i] = make([]int, len(row))
		for j, ch := range row {
			grid.g[i][j], _ = strconv.Atoi(string(ch))
		}
	}
	return
}

func solve(grid Grid) (count int) {
	// Find all gaps
	var gaps []Coord
	for y := 0; y < grid.limit(); y++ {
		for x := 0; x < grid.limit(); x++ {
			if grid.get(y, x) == 0 {
				gaps = append(gaps, Coord{x, y})
			}
		}
	}
	count = iterate(grid, gaps)
	return
}

type Coord struct {
	x, y int
}

func iterate(grid Grid, gaps []Coord) (count int) {
	// Terminate when all gaps are filled
	if len(gaps) == 0 {
		return 1
	}
	// Try all possibilities
	for i := 1; i <= grid.limit(); i++ {
		if grid.test(gaps[0].y, gaps[0].x, i) {
			count += iterate(grid.set(gaps[0].y, gaps[0].x, i), gaps[1:])
		}
	}
	return
}

type Grid struct {
	g [][]int
}

func (grid Grid) limit() int {
	return len(grid.g)
}
func (grid Grid) get(y, x int) int {
	return grid.g[y][x]
}
func (grid Grid) set(Y, X int, item int) (newGrid Grid) {
	newGrid = Grid{make([][]int, grid.limit())}
	for y := 0; y < grid.limit(); y++ {
		newGrid.g[y] = make([]int, grid.limit())
		for x := 0; x < grid.limit(); x++ {
			newGrid.g[y][x] = grid.get(y, x)
		}
	}
	newGrid.g[Y][X] = item
	return
}
func (grid Grid) test(Y, X int, item int) bool {
	for y := 0; y < grid.limit(); y++ {
		if grid.get(y, X) == item {
			return false
		}
	}
	for x := 0; x < grid.limit(); x++ {
		if grid.get(Y, x) == item {
			return false
		}
	}
	return true
}
