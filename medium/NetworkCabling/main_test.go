package main

import (
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"strings"
	"testing"
)

func Test(t *testing.T) {
	for name, testCase := range TestCases {
		t.Run(name, func(t *testing.T) {
			func(testCase TestCase) {
				// Arrange
				reader := strings.NewReader(testCase.input)
				writer := strings.Builder{}

				// Act
				run(reader, &writer)

				// Assert
				assert.That(t, writer.String(), is.EqualTo(testCase.expected), "")
			}(testCase)
		})
	}
}

type TestCase struct {
	input, expected string
}

var TestCases = map[string]TestCase{
	"01": {"3\n0 0\n1 1\n2 2", "4\n"},
	"02": {"3\n1 2\n0 0\n2 2", "4\n"},
	"03": {"4\n1 2\n0 0\n2 2\n1 3", "5\n"},
	"04": {"1\n1 1", "0\n"},
	"05": {"3\n-5 -3\n-9 2\n3 -4", "18\n"},
	"06": {"8\n-28189131 593661218\n102460950 1038903636\n938059973 -816049599\n-334087877 -290840615\n842560881 -116496866\n-416604701 690825290\n19715507 470868309\n846505116 -694479954", "6066790161\n"},
	"11": {"11\n-162526110 -252675912\n-4895917 -240420085\n141008358 -106615672\n206758372 -63665546\n88473194 -37289256\n202531345 73399429\n-135195154 157092065\n171101176 161166515\n-266264470 191334680\n-205060869 233111863\n-137959173 262220087", "2178614523\n"},
}
