package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
)

func main() {
	run(bufio.NewReader(os.Stdin), os.Stdout)
}

func run(reader io.Reader, writer io.Writer) {
	housesX, housesY := read(reader)
	result := solve(housesX, housesY)
	fmt.Fprintln(writer, result) // Write answer to stdout
}

func read(reader io.Reader) (housesX, housesY []int) {
	var N int
	fmt.Fscan(reader, &N)
	fmt.Fprintln(os.Stderr, N)
	housesX = make([]int, N)
	housesY = make([]int, N)

	for i := 0; i < N; i++ {
		var X, Y int
		fmt.Fscan(reader, &X, &Y)
		fmt.Fprintln(os.Stderr, X, Y)
		housesX[i] = X
		housesY[i] = Y
	}
	return
}

func solve(housesX, housesY []int) (cable int) {
	minX, maxX := MINMAX(housesX)
	aveY := MEDIAN(housesY)
	fmt.Fprintln(os.Stderr, aveY, minX, maxX)
	cable = maxX - minX
	for _, house := range housesY {
		cable += ABS(house - aveY)
	}
	return
}

func ABS(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func MEDIAN(x []int) int {
	sort.Ints(x)
	return x[len(x)/2]
}

func MINMAX(x []int) (min, max int) {
	min = x[0]
	max = x[0]
	for _, i := range x[1:] {
		if i < min {
			min = i
		}
		if i > max {
			max = i
		}
	}
	return
}
