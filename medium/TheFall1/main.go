package main

import (
	"fmt"
	"strconv"
	"strings"
)
import "os"
import "bufio"

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Buffer(make([]byte, 1000000), 1000000)

	mp, exit := read(scanner)
	fmt.Fprintln(os.Stderr, mp, exit)
	var XI, YI int
	var POS string
	for {
		scanner.Scan()
		fmt.Sscan(scanner.Text(), &XI, &YI, &POS)
		fmt.Fprintln(os.Stderr, XI, YI, POS)
		var pos int
		switch POS {
		case "LEFT":
			pos = 0
		case "RIGHT":
			pos = 1
		case "TOP":
			pos = 2
		}
		d := routes[mp[YI][XI]][pos]
		x2, y2 := move(XI, YI, d)

		// One line containing the X Y coordinates of the room in which you believe Indy will be on the next turn.
		fmt.Println(x2, y2)
	}
}

func read(scanner *bufio.Scanner) (mp [][]int, exit int) {
	// W: number of columns.
	// H: number of rows.
	var W, H int
	scanner.Scan()
	fmt.Sscan(scanner.Text(), &W, &H)
	mp = make([][]int, H)

	for i := 0; i < H; i++ {
		scanner.Scan()
		LINE := scanner.Text() // represents a line in the grid and contains W integers. Each integer represents one room of a given type.
		mp[i] = parse(LINE)
	}
	// EX: the coordinate along the X axis of the exit (not useful for this first mission, but must be read).
	scanner.Scan()
	fmt.Sscan(scanner.Text(), &exit)
	return
}

func parse(line string) (l2 []int) {
	l := strings.Split(line, " ")
	l2 = make([]int, len(l))
	for i, s := range l {
		l2[i], _ = strconv.Atoi(s)
	}
	return
}

type dir int

const (
	left  dir = iota
	right dir = iota
	down  dir = iota
	ERROR dir = iota
)

// In order L,R,T
var routes = [14][3]dir{
	{ERROR, ERROR, ERROR}, // 0
	{down, down, down},
	{right, left, ERROR},
	{ERROR, ERROR, down},
	{ERROR, down, left}, // 4
	{down, ERROR, right},
	{right, left, ERROR},
	{ERROR, down, down},
	{down, down, ERROR}, // 8
	{down, ERROR, down},
	{ERROR, ERROR, left},
	{ERROR, ERROR, right},
	{ERROR, down, ERROR}, // 12
	{down, ERROR, ERROR},
}

func move(x, y int, d dir) (x2, y2 int) {
	switch d {
	case down:
		x2, y2 = x, y+1
	case left:
		x2, y2 = x-1, y
	case right:
		x2, y2 = x+1, y
	case ERROR:
		panic("Error")
	}
	return
}
