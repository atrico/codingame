package main

import (
	"fmt"
	"os"
)

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

func main() {
	// MapHeight: number of rows.
	// MapWidth: number of columns.
	// A: number of rounds between the time the alarm countdown is activated and the time the alarm goes off.
	var MapHeight, MapWidth, A int
	fmt.Scan(&MapHeight, &MapWidth, &A)
	Debugln("Map = (", MapWidth, " x ", MapHeight, ")")

	target := MapTarget
	var move Move
	for {
		kirk := ReadLocation()
		Debugln("Kirk at ", kirk)
		theMap := ReadMap(MapWidth, MapHeight)
		theMap.Display(kirk)
		if theMap.GetAt(kirk) == MapTarget {
			target = MapStart
		}
		move = theMap.FindMove(kirk, target)
		Debugln("Move: ", move)

		fmt.Println(move) // Kirk's next move (UP DOWN LEFT or RIGHT).
	}
}

func Debug(msg ...interface{}) {
	fmt.Fprint(os.Stderr, msg...)
}

func Debugln(msg ...interface{}) {
	Debug(msg...)
	Debug("\n")
}

type Move string

const (
	MapUnknown rune = '?'
	MapWall    rune = '#'
	//	MapSpace   rune = '.'
	MapStart  rune = 'T'
	MapTarget rune = 'C'

	MapKirk = "X"

	MoveNone  Move = ""
	MoveUp    Move = "UP"
	MoveDown  Move = "DOWN"
	MoveLeft  Move = "LEFT"
	MoveRight Move = "RIGHT"
)

// ----------------------------------------------------------------------------------------------------------------------------
// Location
// ----------------------------------------------------------------------------------------------------------------------------

type Location struct {
	x, y int
}

func (l Location) String() string {
	return fmt.Sprintf("(%d,%d)", l.x, l.y)
}

func ReadLocation() Location {
	var loc Location
	fmt.Scan(&loc.y, &loc.x)
	return loc
}

// ----------------------------------------------------------------------------------------------------------------------------
// Map
// ----------------------------------------------------------------------------------------------------------------------------

type Map struct {
	width, height int
	cells         []string
}

func ReadMap(width, height int) Map {
	m := Map{width: width, height: height, cells: make([]string, height)}
	for i := 0; i < height; i++ {
		fmt.Scan(&m.cells[i])
	}
	return m
}

func (m Map) GetAt(loc Location) rune {
	return rune(m.cells[loc.y][loc.x])
}

func (m Map) Display(kirk Location) {
	for y, row := range m.cells {
		for x, cell := range row {
			content := string(cell)
			if kirk.y == y && kirk.x == x {
				content = MapKirk
			}
			Debug(content)
		}
		Debugln()
	}
}

func (m Map) FindMove(loc Location, target rune) (bestMove Move) {
	visited := newSet()
	pending := newQueue(element{loc, MoveNone})
	bestMove = MoveNone

	// Find target/start
	for !pending.isEmpty() {
		current := pending.pop()
		visited.add(current.loc)
		for _, dir := range []Move{MoveUp, MoveDown, MoveLeft, MoveRight} {
			newEl := current
			if newEl.firstMove == MoveNone {
				newEl.firstMove = dir
			}
			var newCell rune
			newEl.loc, newCell = m.TestMove(current.loc, dir)
			if !visited.contains(newEl.loc) {
				// Check for target
				if newCell == target {
					return newEl.firstMove
				}
				// Check for unknown
				if newCell == MapUnknown && bestMove == MoveNone {
					bestMove = newEl.firstMove
				}
				// Check for way ahead
				if newCell != MapWall && newCell != MapUnknown {
					pending.push(newEl)
				}
				visited.add(newEl.loc)
			}
		}
	}
	return bestMove
}

func (m Map) TestMove(loc Location, move Move) (newLoc Location, cell rune) {
	newLoc = loc
	switch move {
	case MoveUp:
		newLoc.y--
	case MoveDown:
		newLoc.y++
	case MoveLeft:
		newLoc.x--
	case MoveRight:
		newLoc.x++
	}
	if 0 <= newLoc.x && newLoc.x < m.width && 0 <= newLoc.y && newLoc.y < m.height {
		cell = rune(m.cells[newLoc.y][newLoc.x])
	}
	return newLoc, cell
}

// ----------------------------------------------------------------------------------------------------------------------------
// Queue
// ----------------------------------------------------------------------------------------------------------------------------

type element struct {
	loc       Location
	firstMove Move
}

type queue struct {
	Queue []element
	Count int
}

func newQueue(initial ...element) queue {
	q := queue{Queue: make([]element, len(initial)), Count: len(initial)}
	for i, el := range initial {
		q.Queue[i] = el
	}
	return q
}

func (q queue) isEmpty() bool {
	return q.Count == 0
}

func (q *queue) push(el element) {
	q.Queue = append(q.Queue, el)
	q.Count++
}

func (q *queue) pop() element {
	if q.isEmpty() {
		panic("Queue is isEmpty")
	}
	val := q.Queue[0]
	q.Queue = q.Queue[1:]
	q.Count--
	return val
}

// Set
var _blank struct{}

type set map[Location]struct{}

func newSet(initial ...Location) set {
	s := make(set)
	for _, loc := range initial {
		s.add(loc)
	}
	return s
}

func (s *set) add(loc Location) {
	(*s)[loc] = _blank
}

func (s set) contains(loc Location) bool {
	_, ok := s[loc]
	return ok
}
