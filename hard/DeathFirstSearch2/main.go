package main

import (
	"fmt"
	"os"
	"sort"
)

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

func main() {
	// N: the total number of nodes in the level, including the gateways
	// L: the number of Distance
	// E: the number of exit gateways
	var N, L, E int
	fmt.Scan(&N, &L, &E)
	Debug(N, L, E)

	graph := NewGraph(N)

	for i := 0; i < L; i++ {
		// N1: N1 and N2 defines a link between these nodes
		var N1, N2 int
		fmt.Scan(&N1, &N2)
		graph.AddLink(Link{N1, N2})
		Debug(N1, N2)
	}
	for i := 0; i < E; i++ {
		// EI: the index of a gateway Node
		var EI int
		fmt.Scan(&EI)
		graph.SetGateway(EI)
		Debug(EI)
	}
	for {
		// SI: The index of the Node on which the Skynet agent is positioned this turn
		var SI int
		fmt.Scan(&SI)
		Debug("Agent = ", SI)

		// Find agents shortest distance to first "positive" node
		node := graph.GetFirstPositiveNode(SI)
		// Cut (first gateway)
		cutLink := graph.GetGateway(node)

		Debug("Cut = ", cutLink)

		// Example: 0 1 are the indices of the nodes you wish to sever the link between
		fmt.Println(fmt.Sprintf("%d %d", cutLink.Node1, cutLink.Node2))

		// Update graph
		graph.CutLink(cutLink)
	}
}

func Debug(msg ...interface{}) {
	fmt.Fprintln(os.Stderr, msg...)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Graph
// ----------------------------------------------------------------------------------------------------------------------------

type graph []Node

type Node struct {
	IsGateway bool
	Links     IntSet
}

type Link struct {
	Node1, Node2 int
}

type Graph interface {
	// Setup
	AddLink(link Link) Graph
	CutLink(link Link) Graph
	SetGateway(idx int) Graph
	// Solve
	GetNode(idx int) Node
	GetGatewayCount(idx int) int
	GetGateway(idx int) Link
	GetFirstPositiveNode(start int) int
}

func NewGraph(nodes int) Graph {
	g := make(graph, nodes)
	for i := range g {
		g[i] = Node{false, NewIntSet()}
	}
	return &g
}

// ----------------------------------------------------------------------------------------------------------------------------
// Solve
// ----------------------------------------------------------------------------------------------------------------------------
func (g *graph) GetNode(idx int) Node {
	return (*g)[idx]
}

// Number of gateways from this node
func (g *graph) GetGatewayCount(idx int) int {
	count := 0
	for lnk := range g.GetNode(idx).Links {
		if g.GetNode(lnk).IsGateway {
			count++
		}
	}
	return count
}

func (g *graph) GetGateway(start int) Link {
	startNode := g.GetNode(start)
	for lnk := range startNode.Links {
		if (*g)[lnk].IsGateway {
			return Link{start, lnk}
		}
	}
	panic("no gateway here")
}

// Dijkstra like search
// "distance" = gateway links - moves
// Return first positive node
func (g *graph) GetFirstPositiveNode(start int) int {
	// Shortcut
	if g.GetGatewayCount(start) > 0 {
		return start
	}
	visited := NewIntSet(start)
	pending := NewQueue(QueueElement{start, 0})
	highest := start
	highestCount := 0
	for !pending.IsEmpty() {
		// Get highest score
		pending.Sort(func(i, j int) bool { return pending.Queue[i].Distance > pending.Queue[j].Distance })
		current := pending.Pop()
		for lnk := range (*g)[current.Node].Links {
			if !visited.Contains(lnk) {
				// Calculate new "distance"
				gwCount := g.GetGatewayCount(lnk)
				if gwCount > highestCount {
					highest = lnk
					highestCount = gwCount
				}
				distance := current.Distance - 1 + gwCount
				if distance > 0 {
					return lnk
				}
				visited.Add(lnk)
				pending.Push(QueueElement{lnk, distance})
			}
			visited.Add(current.Node)
		}
	}
	return highest
}

// ----------------------------------------------------------------------------------------------------------------------------
// Setup
// ----------------------------------------------------------------------------------------------------------------------------

func (g *graph) AddLink(link Link) Graph {
	(*g)[link.Node1].Links.Add(link.Node2)
	(*g)[link.Node2].Links.Add(link.Node1)
	return g
}

func (g *graph) CutLink(link Link) Graph {
	(*g)[link.Node1].Links.Remove(link.Node2)
	(*g)[link.Node2].Links.Remove(link.Node1)
	return g
}

func (g *graph) SetGateway(idx int) Graph {
	(*g)[idx].IsGateway = true
	return g
}

// ----------------------------------------------------------------------------------------------------------------------------
// Queue
// ----------------------------------------------------------------------------------------------------------------------------

type QueueElement struct {
	Node     int
	Distance int
}
type Queue struct {
	Queue []QueueElement
	Count int
}

func NewQueue(initial ...QueueElement) Queue {
	q := Queue{Queue: make([]QueueElement, len(initial)), Count: len(initial)}
	for i, el := range initial {
		q.Queue[i] = el
	}
	return q
}

func (q Queue) IsEmpty() bool {
	return q.Count == 0
}

func (q *Queue) Push(el QueueElement) {
	q.Queue = append(q.Queue, el)
	q.Count++
}

func (q *Queue) Pop() QueueElement {
	if q.IsEmpty() {
		panic("Queue is IsEmpty")
	}
	val := q.Queue[0]
	q.Queue = q.Queue[1:]
	q.Count--
	return val
}

// Sort queue, lowest value will be popped next
func (q *Queue) Sort(less func(i, j int) bool) {
	sort.Slice(q.Queue, less)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Set
// ----------------------------------------------------------------------------------------------------------------------------
var _blank struct{}

type IntSet map[int]struct{}

func NewIntSet(initial ...int) IntSet {
	s := make(IntSet)
	for _, el := range initial {
		s.Add(el)
	}
	return s
}

func (s *IntSet) Add(el int) {
	(*s)[el] = _blank
}

func (s *IntSet) Remove(el int) {
	delete(*s, el)
}

func (s IntSet) Contains(el int) bool {
	_, ok := s[el]
	return ok
}
