package main

import (
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
	"strings"
	"testing"
)

func Test(t *testing.T) {
	for name, testCase := range TestCases {
		t.Run(name, func(t *testing.T) {
			func(testCase TestCase) {
				// Arrange
				reader := strings.NewReader(testCase.input)
				writer := strings.Builder{}

				// Act
				run(reader, &writer)

				// Assert
				assert.That(t, writer.String(), is.EqualTo(testCase.expected), "")
			}(testCase)
		})
	}
}

type TestCase struct {
	input, expected string
}

var TestCases = map[string]TestCase{
	"1": {"2 1\n1H", ">.\n"},
	"2": {"3 3\n2.X\n..H\n.H1", "v..\nv..\n>.^\n"},
	"3": {"5 5\n4..XX\n.H.H.\n...H.\n.2..2\n.....", "v....\nv...<\nv^..^\nv^.^^\n>>>^.\n"},
	"4": {"6 6\n3..H.2\n.2..H.\n..H..H\n.X.2.X\n......\n3..H..\n", ">>>..v\n.>>>.v\n>>....\n^..v..\n^..v..\n^.....\n"},
	//	"6": {"8 8\n.......4\n....HH.2\n..5.....\nH....22X\n.3XH.HXX\n..X3.H.X\n..XH....\nH2X.H..3", "v<<<<<<<\n"},
	//"19": {"40 10\n"},
}
