package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	run(bufio.NewReader(os.Stdin), os.Stdout)
}

func run(reader io.Reader, writer io.Writer) {
	grid := read(reader)
	//for _, row := range grid {
	//	fmt.Fprintln(os.Stderr, string(row))
	//}

	start := State{grid}
	if result, ok := start.solve(); ok {
		fmt.Fprintln(writer, strings.Join(result.Display(), "\n"))
	} else {
		fmt.Fprintln(os.Stderr, "Could not be solved")
	}
}

func read(reader io.Reader) (start [][]rune) {
	var width, height int
	fmt.Fscan(reader, &width, &height)
	start = make([][]rune, height)

	for i := 0; i < height; i++ {
		var row string
		fmt.Fscan(reader, &row)
		start[i] = []rune(row)
	}
	return
}

func (s State) solve() (result State, ok bool) {
	if x, y, shots := s.findBall(); shots > 0 {
		// Try each move
		for _, mv := range allMoves {
			if newState, ok2 := s.move(x, y, mv); ok2 {
				result, ok = newState.solve()
				if ok {
					return
				}
			}
		}
	} else {
		result = s
		ok = true
	}
	return
}

type State struct {
	grid [][]rune
}

func (s State) Display() (output []string) {
	text := strings.Builder{}
	for _, row := range s.grid {
		text.Reset()
		for _, char := range row {
			switch char {
			case '^', 'v', '<', '>':
				text.WriteString(string(char))
			default:
				text.WriteString(".")
			}
		}
		output = append(output, text.String())
	}
	return
}

func (s State) copy() (s2 State) {
	s2.grid = make([][]rune, len(s.grid))
	for y := range s2.grid {
		s2.grid[y] = make([]rune, len(s.grid[y]))
		for x := range s.grid[y] {
			s2.grid[y][x] = s.grid[y][x]
		}
	}
	return
}

// Find the ball with the most shots remaining
func (s State) findBall() (x, y, shots int) {
	var at rune
	for y = range s.grid {
		for x, at = range s.grid[y] {
			if '1' <= at && at <= '9' {
				shots = int(at - '0')
				return
			}
		}
	}
	return
}

type moveType int

const (
	moveUp    moveType = iota
	moveDown  moveType = iota
	moveLeft  moveType = iota
	moveRight moveType = iota
)

var allMoves = []moveType{moveUp, moveDown, moveLeft, moveRight}

func (s State) move(x, y int, mv moveType) (s2 State, ok bool) {
	shots := int(s.grid[y][x] - '0')
	// Check for valid shots
	if 0 < shots && shots < 10 {
		deltaX, deltaY, char := deltas(mv)
		// Check bounds
		x2 := x + deltaX*shots
		y2 := y + deltaY*shots
		if 0 > x2 || 0 > y2 || y2 > len(s.grid)-1 || x2 > len(s.grid[y2])-1 {
			return
		}
		// Check target position
		if s.grid[y2][x2] != 'H' && (s.grid[y2][x2] != '.' || shots == 1) {
			return
		}
		// Check route
		x2, y2 = x, y
		for i := 0; i < shots; i++ {
			// Invalid location to cross
			if i > 0 && s.grid[y2][x2] != '.' && s.grid[y2][x2] != 'X' {
				return
			}

			x2 += deltaX
			y2 += deltaY
		}
		// Make the move
		s2 = s.copy()
		x2, y2 = x, y
		for i := 0; i < shots; i++ {
			s2.grid[y2][x2] = char

			x2 += deltaX
			y2 += deltaY
		}
		// Invalid target position
		if s.grid[y2][x2] == '.' {
			s2.grid[y2][x2] = rune('0' + (shots - 1))
		} else if s.grid[y2][x2] == 'H' {
			// Hole used
			s2.grid[y2][x2] = 'h'
		}
		ok = true
	}
	return
}

func deltas(mv moveType) (xd, yd int, char rune) {
	xd = 0
	yd = 0
	switch mv {
	case moveUp:
		yd = -1
		char = '^'
	case moveDown:
		yd = 1
		char = 'v'
	case moveLeft:
		xd = -1
		char = '<'
	case moveRight:
		xd = 1
		char = '>'
	}
	return
}
